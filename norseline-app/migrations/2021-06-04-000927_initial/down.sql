-- This file should undo anything in `up.sql`

DROP TABLE ship_blueprint_variants;

DROP TABLE ship_blueprint_variant_join_classification;

DROP TABLE ship_blueprint_classifications;

DROP TABLE ship_blueprints;

DROP TABLE manufacturers;